import axios from 'axios';

const Api = {
  getPosts: async () => {
    try {
      const response = await axios.get(`${process.env.VUE_APP_BASE_URL}/posts?limit=25&page=1`);
      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  },
  getPostDetail: async (id) => {
    try {
      const response = await axios.get(`${process.env.VUE_APP_BASE_URL}/posts/${id}`);
      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  },
  addPost: async (body) => {
    try {
      const response = await axios.post(`${process.env.VUE_APP_BASE_URL}/posts`, body);
      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  },
  updatePost: async (id, body) => {
    try {
      const response = await axios.put(`${process.env.VUE_APP_BASE_URL}/posts/${id}`, body);
      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  },
  deletePost: async (id) => {
    try {
      const response = await axios.delete(`${process.env.VUE_APP_BASE_URL}/posts/${id}`);
      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  },
  readPost: async (id) => {
    try {
      const response = await axios.post(`${process.env.VUE_APP_BASE_URL}/posts/${id}/read`);
      return Promise.resolve(response);
    } catch (err) {
      return Promise.reject(err);
    }
  },
};

export default Api;