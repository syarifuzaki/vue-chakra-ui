import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/post/add',
      name: 'add',
      component: () => import('../views/AddView.vue')
    },
    {
      path: '/post/:id',
      name: 'post',
      component: () => import('../views/PostView.vue')
    },
    {
      path: '/post/:id/update',
      name: 'add',
      component: () => import('../views/UpdateView.vue')
    },
  ]
});

export default router
