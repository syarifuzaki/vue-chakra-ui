import Vue from 'vue'
import Chakra, { CThemeProvider, CReset } from '@chakra-ui/vue'
import App from './App.vue'
import router from './router'
import VueRouter from 'vue-router';

Vue.use(VueRouter)
Vue.use(Chakra)

new Vue({
  el: '#app',
  router: router,
  render: (h) => h(CThemeProvider, [h(CReset), h(App)])
}).$mount()